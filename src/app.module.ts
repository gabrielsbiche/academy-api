import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { join } from 'path';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import {
  DATABASE_HOST,
  DATABASE_PORT,
  DATABASE_USERNAME,
  DATABASE_PASSWORD,
  DATABASE_NAME,
  DATABASE_SYNC,
} from './config';
import { StudentsModule } from './modules/students/students.module';
import { Student } from './modules/students/entities/student.entity';
import { DisciplinesModule } from './modules/disciplines/disciplines.module';
import { Discipline } from './modules/disciplines/entities/discipline.entity';
import { LessonsModule } from './modules/lessons/lessons.module';
import { Lesson } from './modules/lessons/entities/lesson.entity';
import { ContentsModule } from './modules/contents/contents.module';
import { Content } from './modules/contents/entities/content.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: DATABASE_HOST,
      port: DATABASE_PORT,
      username: DATABASE_USERNAME,
      password: DATABASE_PASSWORD,
      database: DATABASE_NAME,
      entities: [Student, Discipline, Lesson, Content],
      synchronize: DATABASE_SYNC,
    }),
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
      sortSchema: true,
      debug: false,
    }),
    StudentsModule,
    DisciplinesModule,
    LessonsModule,
    ContentsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
