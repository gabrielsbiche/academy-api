# Academy API

## Tecnologias utilizadas
NestJs, TypeORM, GraphQL, Nestjs-query, PostgreSQL e Docker.

## Insomnia
Coleção Insomnia disponível na raiz do projeto.

## Pré requisitos 
[Docker](https://www.docker.com/)

[Docker-compose](https://docs.docker.com/compose/install/)

## Siga os passos abaixo para executar o projeto

### 1° Passo - Configure as variáveis de ambiente para conexão com banco de dados
Renomeie o arquivo da raiz do projeto chamado .env.example para .env e defina seus dados de aceso ao banco de dados

### 2° Passo - Execute o Postgres
```
$ docker-compose up
```

### 3° Passo - Instale as dependências 
```
$ yarn install 
```

### 4° Passo - Execute a aplicação
```
$ yarn start:dev 
```

## Modelo relacional do banco de dados

![relate-tables](https://user-images.githubusercontent.com/63760217/208318239-ef259d35-4a29-426c-bba9-cdd1eb995d81.png)




